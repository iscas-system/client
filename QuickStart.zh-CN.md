# 快速使用说明

## libaripple
发布订阅基础服务（ARipple）的客户端，拟支持消息、流式数据、文件、对象存储、关系型数据库等多种类型数据的发布订阅。

目前基于nats二次开发。

### 消息发布/订阅
消息发布订阅功能封装在ARipple命名空间的Client类中。

#### 连接/断开连接
调用`Client`类的`Connect`方法连接至服务器，调用`Disconnect`方法断开连接
- bool Connect();
- bool Disconnect();

例子：连接至服务器localhost:4222
```c++
ARipple::Client client("localhost", 4222);
std::cout << "Address: " << client.GetAddress() << std::endl;
std::cout << "Port: " << client.GetPort() << std::endl;
client.Connect();
// Do something
client.Disconnect();
```

亦可使用无参构造函数，并在执行connect方法前调用`SetAddress`和`SetPort`方法设置服务器地址和参数。

#### 消息发布
调用`Client`类的`Publish`方法向指定主题发布消息。
- bool Publish(const char *subject, const void *data, int length);

参数说明
- subject：主题
- data：待发送数据
- length：待发送数据长度

例子：向主题`test`上发送字符串`hello, world`
```c++
client.Publish("test", "hello, world", strlen("hello, world"));
```

#### 消息订阅/取消订阅
调用`Client`类的`Subscribe`方法接收指定主题上的消息。
- bool Subscribe(const char *subject, MessageHandler messageHandler);

参数说明
- subject：主题
- messageHandler：消息处理回调函数，签名为`bool MessageHandler(const char *data, int length);`，传入参数分别为数据和数据长度。

例子：接收`test`主题上的消息
```c++
bool MessageHandler(const char *data, int length) {
    std::cout << "Receive message: " << data << std::endl;
    return true;
}

client.Subscribe("test", MessageHandler);
```

调用`Client`类的`Unsubscribe`方法取消订阅指定主题
- bool Unsubscribe(const char *subject);

参数说明
- subject：主题

### SQL语句代理

#### 发送SQL语句
调用`Client`类的`SendSql`方法向指定SQLite数据库发布SQL语句，多个数据库副本会代理执行。
- bool SendSql(const char *subject, const char *sql);

参数说明：
- subject：主题
- sql：待发送的SQL语句

例子：向绑定到主题`database.sqlite`的数据库发送SQL语句
```c++
std::string sql =
        "INSERT INTO [message] ([topic], [value]) VALUES (\"message-" + std::to_string(i + 1) +
        "\",\"payload-" + std::to_string(i + 1) + "\");";
if (client.SendSql("database.sqlite", sql.c_str())) {
    std::cout << "Send SQL:" << sql << std::endl;
}
```

#### 同步SQL语句
调用`Client`类的`StartSqlSync`方法同步指定主题上的SQL语句并在本地SQLite数据库上执行。
- bool StartSqlSync(const char *fileName, const char *subject);

参数说明
- fileName：SQLite数据库文件
- subject：绑定的主题

例子：订阅主题`database.sqlite`上的SQL语句并代理执行
```c++
client.StartSqlSync("database.sqlite", "database.sqlite");
```

调用`Client`类的`StopSqlSync`方法取消同步
- bool StopSqlSync(const char *subject);

参数说明
- subject：主题

### 流式数据生产/消费

Clientstream类

用于持久消息订阅发布的类，消息在队列中持久存放，保证用户至少可以接收到一次消息。

#### 构造函数
- Clientstream();
- Clientstream(const char* streamname, const char* consumername);
- Clientstream(const char* consumername);

参数说明，构造函数最多可以有两个参数:
- streamname:设置持久队列的名字，如果没设置使用缺省的持久队列，如果应用需要使用不同的队列存储消息，可以设置名字。
- consumername:设置消费者名字，如果没有设置使用缺省消费者名字，对于同一个消息，同一个消费者仅可以从队列获取一次。如果不同的应用都需要获取同一个消息，请设置自己的消费者名字。

#### 连接nats服务器
- void connectToServer();
- void connectToServer(const std::string& address);
参数说明
- address：服务器地址，格式是IP:port(如127.0.0.1:4222)。没有参数缺省连接本地服务器地址：127.0.0.1:4222。

#### 发布消息
- void publish(const char* subject, const void* data, int len);
- void publish(natsMsg *msg);

参数说明：
- subject:发布消息主题名字，字符串
- data:发布消息的缓冲区地址
- len:发布消息的长度
- msg:发布的消息结构，包含主题、消息内容、消息长度等，详细见struct message

#### PULL方式订阅消息
- void PullSubscribe(const char* subject);

参数说明：
- subject:订阅消息主题名字，字符串

#### 接收订阅（PULL方式）的消息
- int receiveMessages(void** data,int* len);
- int receiveMessages(natsMsg **msg);

参数说明：
- data:接收到的消息缓冲区地址，使用结束后需要free释放
- len:接收消息的长度
- msg:接收到的消息结构，包含主题、消息内容、消息长度等，使用后需要destroy释放

返回值：
- 返回1：接收消息成功
- 返回0：没有接受到消息，队列中消息已经全部接收

#### PUSH方式订阅消息
- void PushSubscribe(const char* subject, MsgHandler handler);
- void setMsgHandler(MsgHandler handler);

参数说明：
- subject:订阅消息主题名字，字符串
- handler:处理消息的回调函数入口，订阅后如果队列中有消息，自动调用设置的回调函数处理消息

### 文件流

Clientfile类

用于将文件发送到持久队列中，并从持久队列中接收文件

#### 构造函数
- Clientfile();
- Clientfile(const char* consumername);

参数说明：
- consumername:设置获取文件消费者名字，如果没有设置使用缺省消费者名字，对于同一个文件，同一个消费者仅可以从队列获取一次。如果不同的应用都需要获取同一个文件，请设置自己的消费者名字。

#### 连接nats服务器
- void connectToServer();
- void connectToServer(const std::string& address);

参数说明：
- address：服务器地址，格式是IP:port(如127.0.0.1:4222)。没有参数缺省连接本地服务器地址：127.0.0.1:4222。

#### 发送文件
- void sendfile(std::string& filePath);

参数说明：
- filePath：需要发送的文件包含路径的名字，当前nats最大消息大约37M以内，因此设置大于35M的文件，提示文件过大

#### 接收文件
- std::string recvfile(std::string& path);

参数说明：
- path：存放接收文件的路径。注意不包含文件名，文件根据发送端文件名自动储存指定的路径。

返回值：
- string：接收到文件的文件名

### 异常处理

Exception类

内部出错后抛出的异常类

#### 获取错误信息
- char* what()

返回值：
- catch到异常Exception后，调用成员函数what()可以获取详细的错误说明
