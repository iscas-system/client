// Author: Zhen Tang <tangzhen12@otcaix.iscas.ac.cn>
// Affiliation: Institute of Software, Chinese Academy of Sciences

#include <cstring>
#include <vector>
#include <nats/nats.h>

#include "sqlite3.h"
#include "ClientImpl.h"
#include "Logger.h"

namespace ARipple {
    Client::ClientImpl::ClientImpl() {
        Logger::Info("ClientImpl", "ClientImpl created.");
        this->port = -1;
        this->connection = nullptr;
        this->connected = false;
    }

    Client::ClientImpl::ClientImpl(const std::string &address, int port) : ClientImpl() {
        this->SetAddress(address);
        this->SetPort(port);
    }

    Client::ClientImpl::~ClientImpl() {
        Logger::Info("~ClientImpl", "ClientImpl destroyed.");
        if (this->subscriptionMap.size() > 0) {
            std::vector<std::string> subjectList;
            for (const auto &elem: this->subscriptionMap) {
                subjectList.push_back(elem.first);
            }
            for (auto elem: subjectList) {
                this->Unsubscribe(elem.c_str());
            }
        }
        if (this->connected) {
            this->Disconnect();
        }
    }

    const std::string &Client::ClientImpl::GetAddress() const {
        return this->address;
    }

    void Client::ClientImpl::SetAddress(std::string address) {
        this->address = std::move(address);
    }

    int Client::ClientImpl::GetPort() const {
        return this->port;
    }

    void Client::ClientImpl::SetPort(int port) {
        this->port = port;
    }

    bool Client::ClientImpl::Connect() {
        std::string url = "nats://";
        url += this->GetAddress();
        url += ":";
        url += std::to_string(this->GetPort());
        natsStatus status = natsConnection_ConnectTo(&this->connection, url.c_str());
        this->connected = (status == NATS_OK);
        Logger::Info("Connect", "Connect to %s: %s.", url.c_str(), this->connected ? "true" : "false");
        return this->connected;
    }

    bool Client::ClientImpl::Disconnect() {
        if (this->connected) {
            natsConnection_Destroy(this->connection);
            this->connected = false;
            this->connection = nullptr;
        }
        return true;
    }

    bool Client::ClientImpl::Publish(const char *subject, const void *data, int length) {
        natsStatus status = natsConnection_Publish(this->connection, subject, data, length);
        return (status == NATS_OK);
    }

    void
    Client::ClientImpl::OnReceivingMessage(natsConnection *nc, natsSubscription *sub, natsMsg *msg, void *closure) {
        const char *data = natsMsg_GetData(msg);
        int length = natsMsg_GetDataLength(msg);
        MessageHandler messageHandler = (MessageHandler) closure;
        messageHandler(data, length);
        natsMsg_Destroy(msg);
    }

    bool Client::ClientImpl::Subscribe(const char *subject, Client::MessageHandler messageHandler) {
        natsSubscription *subscription = nullptr;
        natsStatus status = natsConnection_Subscribe(&subscription, this->connection, subject,
                                                     ClientImpl::OnReceivingMessage, (void *) messageHandler);
        if (status == NATS_OK) {
            if (this->subscriptionMap.find(std::string(subject)) != this->subscriptionMap.end()) {
                Logger::Info("Subscribe", "Subject %s already subscribed, unsubscribing it.", subject);
                this->Unsubscribe(subject);
            }
            this->subscriptionMap[subject] = subscription;
        }
        return (status == NATS_OK);
    }

    bool Client::ClientImpl::Unsubscribe(const char *subject) {
        natsSubscription *subscription = this->subscriptionMap[subject];
        if (subscription != nullptr) {
            natsStatus status = natsSubscription_Unsubscribe(subscription);
            if (status == NATS_OK) {
                this->subscriptionMap.erase(subject);
            }
            return (status == NATS_OK);
        } else {
            return false;
        }
    }

    bool Client::ClientImpl::StartSqlSync(const char *fileName, const char *subject) {
        sqlite3 *database = nullptr;
        bool success = Client::ClientImpl::OpenDatabase(&database, fileName);
        if (!success) {
            return false;
        }
        this->databaseMap[subject] = database;

        natsSubscription *subscription = nullptr;
        natsStatus status = natsConnection_Subscribe(&subscription, this->connection, subject,
                                                     ClientImpl::OnReceivingSql, (void *) database);
        if (status == NATS_OK) {
            if (this->databaseSubscriptionMap.find(std::string(subject)) != this->databaseSubscriptionMap.end()) {
                Logger::Info("StartSqlSync", "Subject %s already subscribed, stop it.", subject);
                this->StopSqlSync(subject);
            }
            this->databaseSubscriptionMap[subject] = subscription;
        }
        return (status == NATS_OK);
    }

    void
    Client::ClientImpl::OnReceivingSql(natsConnection *nc, natsSubscription *sub, natsMsg *msg, void *closure) {
        const char *data = natsMsg_GetData(msg);
        int length = natsMsg_GetDataLength(msg);
        sqlite3 *database = (sqlite3 *) closure;
        Logger::Info("OnReceivingSql", "Received SQL: %.*s", length, data);
        Client::ClientImpl::DoExecuteStatement(database, data);
        natsMsg_Destroy(msg);
    }

    bool Client::ClientImpl::StopSqlSync(const char *subject) {
        sqlite3 *database = this->databaseMap[subject];
        if (database != nullptr) {
            sqlite3_close_v2(database);
            natsSubscription *subscription = this->databaseSubscriptionMap[subject];
            if (subscription != nullptr) {
                natsStatus status = natsSubscription_Unsubscribe(subscription);
                if (status == NATS_OK) {
                    this->databaseSubscriptionMap.erase(subject);
                }
                return (status == NATS_OK);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    bool Client::ClientImpl::SendSql(const char *subject, const char *sql) {
        natsStatus status = natsConnection_Publish(this->connection, subject, sql, strlen(sql));
        return (status == NATS_OK);
    }

    void Client::ClientImpl::DoExecuteStatement(sqlite3 *database, const char *statement) {
        char *errmsg = nullptr;
        int ret = sqlite3_exec(database, statement, ClientImpl::SqliteCallback, nullptr, &errmsg);
        if (ret != SQLITE_OK) {
            Logger::Error("DoExecuteStatement", "SQL error: %s.", errmsg);
            sqlite3_free(errmsg);
        }
    }

    bool Client::ClientImpl::OpenDatabase(sqlite3 **database, const char *fileName) {
        int ret = sqlite3_open_v2(fileName, database,
                                  SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX, nullptr);
        if (ret) {
            Logger::Error("OpenDatabase", "Can't open database: %s.", sqlite3_errmsg(*database));
            return false;
        }
        return true;
    }

    int Client::ClientImpl::SqliteCallback(void *arg, int col, char **str, char **name) {
        Logger::Info("SqliteCallback", "Receiving SELECT statement.");
        int i = 0;
        for (i = 0; i < col; i++) {
            Logger::Info("SqliteCallback", "-> %s = %s ", name[i], str[i]);
        }
        printf("\n");
        return SQLITE_OK;
    }
} // ARipple