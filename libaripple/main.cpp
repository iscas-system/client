#include <iostream>
#include <cstring>
#include <unistd.h>

#include "clientstream.h"
#include "Clientfile.h"

using namespace ARipple;
// 消息处理回调函数示例

MessageHandler handler = [](Message msg) {
    std::cout << "Handled message on subject: " << msg.getSubject() << " with data size: " << msg.getData().size() << ".\n";
    std::cout << "push-subscribing to JetStream: " << msg.getData().data()<<".\n";
    };

// 测试 ClientStream 的 PULL 流程
void testClientStreamPull() {
    try {
        Clientstream stream("stream1", "consumer1");
        stream.connectToServer("127.0.0.1:4222");
        stream.PullSubscribe("test1.subject");
        // char data[] = "Hello, World! pull subscribe";
        // stream.publish("test1.subject", data, sizeof(data));
        Clientstream stream2("stream1", "consumer2");
        stream2.connectToServer("127.0.0.1:4222");
        stream2.PullSubscribe("test1.subject");
        void* receivedData;
        int len;
        stream.receiveMessages(&receivedData, &len);
        std::cout << "pull subscribing to JetStream: " << (char *)receivedData<<".\n";
        stream2.receiveMessages(&receivedData, &len);
        std::cout << "pull subscribing to JetStream: " << (char *)receivedData<<".\n";

 //       Message* receivedMsg;
 //       stream.receiveMessages(&receivedMsg);

 //       delete receivedMsg;

    } catch (const Exception& e) {
        std::cerr << "Exception caught in testClientStreamPull: " << e.what() << std::endl;
    }
}

// 测试 ClientStream 的 PUSH 流程
void testClientStreamPush() {
    try {
        Clientstream stream("stream1", "consumer2");
        stream.connectToServer("127.0.0.1:4222");

        stream.PushSubscribe("test1.subject", handler);
        Clientstream stream1("stream1", "consumer3");
        stream1.connectToServer("127.0.0.1:4222");
        stream1.PushSubscribe("test1.subject", handler);
        Clientstream stream2("stream1", "consumer2");
        stream2.connectToServer("127.0.0.1:4222");

        char data[] = "Hello, World! push subscribe";
        stream2.publish("test1.subject", data, sizeof(data));

    } catch (const Exception& e) {
        std::cerr << "Exception caught in testClientStreamPush: " << e.what() << std::endl;
    }
    sleep(3);
}



// 测试 ClientFile 的流程
void testClientFile() {
    try {
        Clientfile fileClient("file_consumer");
        fileClient.connectToServer("127.0.0.1:4222");

        std::string filePath = "/home/yuhj/filetest/source/test.tar";
        fileClient.sendfile(filePath);

        Clientfile fileClient2("file_consumer");
        fileClient2.connectToServer("127.0.0.1:4222");
        std::string recvPath = "/home/yuhj/filetest/dest";
        std::string receivedFile = fileClient2.recvfile(recvPath);
        std::cout << "receive file: " << receivedFile<<".\n";

    } catch (const Exception& e) {
        std::cerr << "Exception caught in testClientFile: " << e.what() << std::endl;
    }
}

int main() {
    // 运行不同的测试流程
    // testClientStreamPull();
    testClientStreamPush();
    // testClientFile();


    return 0;
}














/*

MessageHandler handler = [](Message msg) {
    printf("push-subscribing to JetStream: %s\n", (char*)msg.getData().data());
};

using namespace ARipple;
int main(int argc, char * argv[])
{
    int i = 1;//0测试发送接收文件，1测试发送接收消息
    if(i == 0) {
        Clientfile cfile("fileconsumer");
        try {
            cfile.connectToServer();
            std::string filename = "/home/yuhj/filetest/source/test.tar";
            cfile.sendfile(filename);

            std::string destpath = "/home/yuhj/filetest/dest";
            cfile.recvfile(destpath);
        }
        catch (const Exception& e) {
            printf("Last error is: %s\n",e.what());
        }
    }

    if(i == 1) {
        //    c.connectToServer();
        int pull = 0; // 1测试 pull 模式订阅，0测试 push 模式订阅

        int nlen;
        void* data;
        Clientstream c;
        try {
            c.connectToServer("127.0.0.1:4222");
            //        c.connectToServer("137.0.0.1:4222");
        }
        catch (const Exception& e) {
            printf("Last error is: %s\n",e.what());
        }
        if (pull == 0) {
            c.PushSubscribe("test5", handler);
        }
        unsigned char pubData[20];
        memset((void*)pubData,0x08,20);
        //    c.publish("test4",pubData,20);
        c.publish("test5","2024/07/18 test flow test2 stream",strlen("2024/07/18 test flow test1 stream"));

        if (pull == 1){
            c.PullSubscribe("test5");

            while (c.receiveMessages(&data, &nlen))
            {
                printf("subscribing to JetStream: %s\n", (char *)data);
            };
        }
    }



    std::cout << "Hello, World!" << std::endl;
    return 0;
}
*/