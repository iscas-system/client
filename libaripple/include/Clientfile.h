//
// Created by yuhj on 24-7-23.
//

#ifndef CLIENTFILE_H
#define CLIENTFILE_H

#include "clientstream.h"
#include <fstream>
using namespace ARipple;

class Clientfile {
public:
    Clientfile(); //= default;
    Clientfile(const char* consumername);

    ~Clientfile() noexcept;

    void connectToServer();
    void connectToServer(const std::string& address);
    void sendfile(std::string& filePath);
    std::string recvfile(std::string& path, bool ack = true);

private:
    const char*         m_filestream="filestream";
    const char*         m_filetopic="file_topic";
    const char*         m_fileconsumer;

    Clientstream        m_fileclient;


    void readFile(const std::string& filePath, std::vector<char>& buffer);
    void saveFile(const std::string& fileName, const std::string& directory, const char* fileData, long fileSize);
    std::string ensureTrailingSlash(const std::string& path);
};


#endif //CLIENTFILE_H
