//
// Created by root on 2024/7/17.
//


#ifndef CLOUDMQ_H
#define CLOUDMQ_H

#pragma once

#include <memory>
#include <vector>
#include <string>
#include <unordered_map>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <nats/nats.h>
#include <map>

namespace ARipple {
    using MessageHeaders = std::unordered_multimap<std::string, std::string>;

    enum class ConnectionStatus
    {
        Disconnected = NATS_CONN_STATUS_DISCONNECTED,
        Connecting = NATS_CONN_STATUS_CONNECTING,
        Connected = NATS_CONN_STATUS_CONNECTED,
        Closed = NATS_CONN_STATUS_CLOSED,
        Reconnecting = NATS_CONN_STATUS_RECONNECTING,
        DrainingSubs = NATS_CONN_STATUS_DRAINING_SUBS,
        DrainingPubs = NATS_CONN_STATUS_DRAINING_PUBS
    };

    class Exception : public std::exception
    {
    public:
        Exception(natsStatus s) : errorCode(s), errorText(natsStatus_GetText(s)) {}
        Exception(std::string s) : errorCode(NATS_ERR),errorText(s) {}
        virtual const char* what() const noexcept override { return errorText.c_str(); }

        const natsStatus errorCode;
    private:
        std::string errorText;
    };

    struct Options
    {
        std::vector<std::string> servers;
        std::string user;
        std::string password;
        std::string token;
        bool randomize = true;
        int64_t timeout;
        std::string name;
        bool secure = false;
        bool verbose = false;
        bool pedantic = false;
        int64_t pingInterval;
        int maxPingsOut;
        int ioBufferSize;
        bool allowReconnect = true;
        int maxReconnect;
        int64_t reconnectWait;
        int reconnectBufferSize;
        int maxPendingMessages;
        bool echo = true;

        Options();
    };

    class Message {
    public:
        Message();
        // 构造函数：使用 std::vector<uint8_t>
        Message(const std::vector<uint8_t>& data, const std::string& subject);
        // 构造函数：使用 void* 数据并转换为 std::vector<uint8_t>
        Message(const void* dataPtr, size_t dataLength, const std::string& subject);
        // 构造函数：从 natsMsg 对象创建 Message 实例
        Message(natsMsg* msg);

        // 获取消息数据
        std::vector<uint8_t> getData() const;

        // 获取消息主题
        std::string getSubject() const;

        // 获取回复主题
        std::string getReply() const;

        // 设置消息头
        void setHeader(const std::string& key, const std::string& value);
        std::string getHeaderValue(const std::string& key) const;

        // 将 Message 对象转换为 natsMsg 对象
        natsStatus toNatsMsg(natsMsg** msg) const;

        ~Message();

    private:
        std::vector<uint8_t> data;
        std::string subject;
        std::string reply;
        natsConnection* conn;
        natsOptions* opts;
        std::map<std::string, std::string> headers; // 保存消息头
    };
    // 用户回调函数类型
    typedef void (*MessageHandler)(Message msg);


    struct JsOptions
    {
        std::string domain;
        int64_t timeout = 5000;
    };

    class Clientstream
    {
    public:
        Clientstream(); //= default;
        Clientstream(const char* streamname, const char* consumername);
        Clientstream(const char* consumername);

        ~Clientstream() noexcept;

        void connectToServer();
        void connectToServer(const std::string& address);

        void publish(const char* subject, const void* data, int len);
        void publish(Message *msg);

        void PullSubscribe(const char* subject);
        int receiveMessages(void** data,int* len);
        int receiveMessages(Message **msg,bool ack = true);
        void PushSubscribe(const char* subject, MessageHandler handler);

        natsConnection* getNatsConnection() const { return m_conn; }
        std::string currentServer() const;
        void setstreamname(const char* streamname){m_streamname = streamname;}
        void setconsumername(const char* consumername){m_consumername = consumername;}

        void close() noexcept;




    private:
        natsConnection*     m_conn = nullptr;
        jsCtx*              m_jsCtx = nullptr;
        jsOptions           m_jsopts;
        jsStreamInfo        *m_jsinfo = nullptr;
        jsPubAck            *m_pubAck = nullptr;
        jsConsumerInfo      *m_consumerinfo = nullptr;
        natsSubscription    *m_sub = nullptr;
        jsSubOptions        m_subOpts;

        const char*         m_streamname;
        const char*         m_consumername;



        std::mutex mtx;
        std::condition_variable cv;

        void connectToServer(const Options& opts);
        static void closedConnectionHandler(natsConnection* nc, void* closure);
        void addSubjectIfNotExists(const char* subject);
        void addSubjectToConsumerIfNotExists(const char* newSubject, const int mode);
        bool isSubjectOverlap(const char* existingSubject, const char* newSubject);

        void setMsgHandler(MessageHandler handler);
        // 保存用户的回调函数
        MessageHandler m_msgHandler = nullptr;
        // 内部回调函数
        static void internalMsgHandler(natsConnection* nc, natsSubscription* sub, natsMsg* msg, void* closure);
   };
    void checkError(natsStatus s,jsErrCode jerr=(jsErrCode)0);


}

#endif //CLOUDMQ_H


