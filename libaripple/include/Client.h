// Author: Zhen Tang <tangzhen12@otcaix.iscas.ac.cn>
// Affiliation: Institute of Software, Chinese Academy of Sciences

#ifndef LIBARIPPLE_CLIENT_H
#define LIBARIPPLE_CLIENT_H

#include <string>
#include <memory>
#include <sqlite3.h>

namespace ARipple {

    class Client {
    public:
        typedef bool (*MessageHandler)(const char *data, int length);

        Client();

        Client(const std::string &address, int port);

        virtual ~Client();

        Client(const Client &) = delete;

        Client &operator=(const Client &) = delete;

        const std::string &GetAddress() const;

        void SetAddress(std::string address);

        int GetPort() const;

        void SetPort(int port);

        bool Connect();

        bool Disconnect();

        bool Publish(const char *subject, const void *data, int length);

        bool Subscribe(const char *subject, MessageHandler messageHandler);

        bool Unsubscribe(const char *subject);

        bool StartSqlSync(const char *fileName, const char *subject);

        bool StopSqlSync(const char *subject);

        bool SendSql(const char *subject, const char *sql);

    private:
        class ClientImpl;

        std::unique_ptr<ClientImpl> clientImpl;

    };

} // ARipple

#endif //LIBARIPPLE_CLIENT_H
