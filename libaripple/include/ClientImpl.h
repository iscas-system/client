// Author: Zhen Tang <tangzhen12@otcaix.iscas.ac.cn>
// Affiliation: Institute of Software, Chinese Academy of Sciences

#ifndef LIBARIPPLE_CLIENTIMPL_H
#define LIBARIPPLE_CLIENTIMPL_H

#include <map>
#include <nats/nats.h>
#include "sqlite3.h"
#include "Client.h"

namespace ARipple {

    class Client::ClientImpl {
    public:
        ClientImpl();

        ClientImpl(const std::string &address, int port);

        virtual ~ClientImpl();

        ClientImpl(const Client &) = delete;

        ClientImpl &operator=(const Client &) = delete;

        const std::string &GetAddress() const;

        void SetAddress(std::string addr);

        int GetPort() const;

        void SetPort(int port);

        bool Connect();

        bool Disconnect();

        bool Publish(const char *subject, const void *data, int length);

        bool Subscribe(const char *subject, MessageHandler messageHandler);

        bool Unsubscribe(const char *subject);

        bool StartSqlSync(const char *fileName, const char *subject);

        bool StopSqlSync(const char *subject);

        bool SendSql(const char *subject, const char *sql);

    private:
        std::string address;
        natsConnection *connection;
        bool connected;
        int port;
        std::map<std::string, natsSubscription *> subscriptionMap;
        std::map<std::string, sqlite3 *> databaseMap;
        std::map<std::string, natsSubscription *> databaseSubscriptionMap;

        static void OnReceivingMessage(natsConnection *nc, natsSubscription *sub, natsMsg *msg, void *closure);

        static void OnReceivingSql(natsConnection *nc, natsSubscription *sub, natsMsg *msg, void *closure);

        static bool OpenDatabase(sqlite3 **database, const char *fileName);

        static int SqliteCallback(void *arg, int col, char **str, char **name);

        static void DoExecuteStatement(sqlite3 *database, const char *statement);
    };

} // ARipple

#endif //LIBARIPPLE_CLIENTIMPL_H
