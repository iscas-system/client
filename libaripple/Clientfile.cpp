//
// Created by yuhj on 24-7-23.
//

#include "Clientfile.h"

using namespace ARipple;

Clientfile::Clientfile() : Clientfile("fileconsumer")
{

}



Clientfile::Clientfile(const char* consumername) : m_filestream("filestream"), m_fileconsumer(consumername),m_fileclient(m_filestream,m_fileconsumer)
{
//initialize

}

Clientfile::~Clientfile()
{
//    close();
}

void Clientfile::connectToServer() {
    m_fileclient.connectToServer();
}

void Clientfile::connectToServer(const std::string& address) {
    m_fileclient.connectToServer(address);
}

void Clientfile::readFile(const std::string& filePath, std::vector<char>& buffer) {
    std::ifstream file(filePath, std::ios::binary | std::ios::ate);
    if (!file) {
        throw(Exception("File opening failed for"+filePath));
   }

    std::streamsize size = file.tellg();
    if(size > 67000000) {
        throw(Exception("File size over 67000000"));
    }
    file.seekg(0, std::ios::beg);

    buffer.resize(size);
    if (!file.read(buffer.data(), size)) {
        std::string s = "File reading failed forr"+filePath;
        throw(Exception(s));
    }
}

void Clientfile::sendfile(std::string& filePath) {

    std::vector<uint8_t> fileBuffer;
    readFile(filePath,reinterpret_cast<std::vector<char>&>(fileBuffer));
    std::string fileName = filePath.substr(filePath.find_last_of("/\\") + 1);

    Message msg(fileBuffer,m_filetopic);
    msg.setHeader("FileName", fileName);
    msg.setHeader("FileSize", std::to_string(fileBuffer.size()));

    m_fileclient.publish(&msg);
}


std::string Clientfile::recvfile(std::string& path, bool ack) {
    m_fileclient.PullSubscribe(m_filetopic);

    Message* msg = nullptr;
    if(!m_fileclient.receiveMessages(&msg, ack)) {
    //没有接收到消息
    //    throw(Exception("No documents to receive"));
        return "";
    }

    std::string fileName(msg->getHeaderValue("FileName"));

    long fileSize = std::stol(msg->getHeaderValue("FileSize"));

    saveFile(fileName, path, (const char*)msg->getData().data(), fileSize);

    delete(msg);

    return fileName;
}

// 检查路径是否以斜杠结尾，如果没有则添加
std::string Clientfile::ensureTrailingSlash(const std::string& path) {
    if (path.empty()) return path;
    if (path.back() != '/') {
        return path + '/';
    }
    return path;
}

void Clientfile::saveFile(const std::string& fileName, const std::string& directory, const char* fileData, long fileSize) {
    std::string fullPath = ensureTrailingSlash(directory) + fileName;
    std::ofstream file(fullPath, std::ios::binary);
    if (!file) {
        throw Exception("File opening failed for " + fullPath);
    }

    file.write(fileData, fileSize);
    file.close();
}