// Author: Zhen Tang <tangzhen12@otcaix.iscas.ac.cn>
// Affiliation: Institute of Software, Chinese Academy of Sciences

#include <utility>

#include "Client.h"
#include "ClientImpl.h"
#include "Logger.h"

namespace ARipple {

    Client::Client() {
        this->clientImpl.reset(new ClientImpl());
    }

    Client::Client(const std::string &address, int port) {
        this->clientImpl.reset(new ClientImpl(address, port));
    }

    Client::~Client() {
        Logger::Info("~Client", "Client destroyed.");
    }

    const std::string &Client::GetAddress() const {
        return this->clientImpl->GetAddress();
    }

    void Client::SetAddress(std::string address) {
        this->clientImpl->SetAddress(std::move(address));
    }

    int Client::GetPort() const {
        return this->clientImpl->GetPort();
    }

    void Client::SetPort(int port) {
        this->clientImpl->SetPort(port);
    }

    bool Client::Connect() {
        return this->clientImpl->Connect();
    }

    bool Client::Disconnect() {
        return this->clientImpl->Disconnect();
    }

    bool Client::Publish(const char *subject, const void *data, int length) {
        return this->clientImpl->Publish(subject, data, length);
    }

    bool Client::Subscribe(const char *subject, Client::MessageHandler messageHandler) {
        return this->clientImpl->Subscribe(subject, messageHandler);
    }

    bool Client::Unsubscribe(const char *subject) {
        return this->clientImpl->Unsubscribe(subject);
    }

    bool Client::StartSqlSync(const char *fileName, const char *subject) {
        return this->clientImpl->StartSqlSync(fileName, subject);
    }

    bool Client::StopSqlSync(const char *subject) {
        return this->clientImpl->StopSqlSync(subject);
    }

    bool Client::SendSql(const char *subject, const char *sql) {
        return this->clientImpl->SendSql(subject, sql);
    }
} // ARipple