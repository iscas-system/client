//
// Created by root on 2024/7/17.
//

#include "clientstream.h"
#include "cstring"
#include "opt.h"

using namespace ARipple;



Options::Options()
{
    timeout = NATS_OPTS_DEFAULT_TIMEOUT;
    pingInterval = NATS_OPTS_DEFAULT_PING_INTERVAL;
    maxPingsOut = NATS_OPTS_DEFAULT_MAX_PING_OUT;
    ioBufferSize = NATS_OPTS_DEFAULT_IO_BUF_SIZE;
    maxReconnect = NATS_OPTS_DEFAULT_MAX_RECONNECT;
    reconnectWait = NATS_OPTS_DEFAULT_RECONNECT_WAIT;
    reconnectBufferSize = NATS_OPTS_DEFAULT_RECONNECT_BUF_SIZE;
    maxPendingMessages = NATS_OPTS_DEFAULT_MAX_PENDING_MSGS;
}

static natsOptions* buildNatsOptions(const Options& opts)
{
    natsOptions* o;
    natsOptions_Create(&o);

    if (!opts.servers.empty()) {
        std::vector<const char*> ptrs;
        for (const auto& url : opts.servers) {
            ptrs.push_back(url.c_str());
        }
        checkError(natsOptions_SetServers(o, ptrs.data(), static_cast<int>(ptrs.size())));
    }
    checkError(natsOptions_SetUserInfo(o, opts.user.c_str(), opts.password.c_str()));
    checkError(natsOptions_SetToken(o, opts.token.c_str()));
    checkError(natsOptions_SetNoRandomize(o, !opts.randomize));
    checkError(natsOptions_SetTimeout(o, opts.timeout));
    checkError(natsOptions_SetName(o, opts.name.c_str()));
    checkError(natsOptions_SetVerbose(o, opts.verbose));
    checkError(natsOptions_SetPedantic(o, opts.pedantic));
    checkError(natsOptions_SetPingInterval(o, opts.pingInterval));
    checkError(natsOptions_SetMaxPingsOut(o, opts.maxPingsOut));
    checkError(natsOptions_SetAllowReconnect(o, opts.allowReconnect));
    checkError(natsOptions_SetMaxReconnect(o, opts.maxReconnect));
    checkError(natsOptions_SetReconnectWait(o, opts.reconnectWait));
    checkError(natsOptions_SetReconnectBufSize(o, opts.reconnectBufferSize));
    checkError(natsOptions_SetMaxPendingMsgs(o, opts.maxPendingMessages));
    checkError(natsOptions_SetNoEcho(o, !opts.echo));

    return o;
}


Message::Message() {
}

Message::Message(const std::vector<uint8_t>& data, const std::string& subject)
        : data(data), subject(subject) {}

Message::Message(const void* dataPtr, size_t dataLength, const std::string& subject)
    : data(reinterpret_cast<const uint8_t*>(dataPtr), reinterpret_cast<const uint8_t*>(dataPtr) + dataLength),
      subject(subject) {}

Message::Message(natsMsg* msg) {
    data.assign(natsMsg_GetData(msg), natsMsg_GetData(msg) + natsMsg_GetDataLength(msg));
    subject = natsMsg_GetSubject(msg);
    const char* replyPtr = natsMsg_GetReply(msg);
    reply = replyPtr ? std::string(replyPtr) : "";

    // 初始化消息头
    const char** keys = nullptr;
    int count = 0;
    natsStatus s = natsMsgHeader_Keys(msg, &keys, &count);
    if (s == NATS_OK) {
        for (int i = 0; i < count; ++i) {
            const char* key = keys[i];
            const char* value = nullptr;
            natsMsgHeader_Get(msg, key, &value);
            headers[key] = value ? value : "";
        }
        free((void*)keys);
    }
}

    // 获取消息数据
std::vector<uint8_t> Message::getData() const {
    return data;
}

// 获取消息主题
std::string Message::getSubject() const {
    return subject;
}

// 获取回复主题
std::string Message::getReply() const {
    return reply;
}

// 设置消息头
void Message::setHeader(const std::string& key, const std::string& value) {
    headers[key] = value;
}
std::string Message::getHeaderValue(const std::string& key) const {
    auto it = headers.find(key);
    if (it != headers.end()) {
        return it->second;
    }
    return ""; // 或者返回一个可选指针 nullptr 表示未找到
}
// 将 Message 对象转换为 natsMsg 对象
natsStatus Message::toNatsMsg(natsMsg** msg) const {
    natsStatus status = natsMsg_Create(msg, subject.c_str(), reply.empty() ? NULL : reply.c_str(), reinterpret_cast<const char*>(data.data()), data.size());
    if (status == NATS_OK) {
        for (const auto& header : headers) {
            status = natsMsgHeader_Set(*msg, header.first.c_str(), header.second.c_str());
            if (status != NATS_OK) {
                natsMsg_Destroy(*msg);
                *msg = nullptr;
                break;
            }
        }
    }
    return status;
}

// 析构函数，销毁 natsMsg 对象
Message::~Message() {
}


static void errorHandler(natsConnection* /*nc*/, natsSubscription* /*subscription*/, natsStatus err, void* closure) {
    // Clientstream* c = reinterpret_cast<Clientstream*>(closure);
    // emit c->errorOccurred(err, getNatsErrorText(err)); // Adapt this part to your desired callback method
}

void Clientstream::closedConnectionHandler(natsConnection* /*nc*/, void *closure) {
/*    Clientstream* c = reinterpret_cast<Client*>(closure);
    // emit c->statusChanged(ConnectionStatus::Closed); // Adapt this part to your desired callback method
    std::lock_guard<std::mutex> lock(c->mtx);
    c->closed = true;
    c->cv.notify_all();*/
}

static void reconnectedHandler(natsConnection* /*nc*/, void *closure) {
    // Clientstream* c = reinterpret_cast<Clientstream*>(closure);
    // emit c->statusChanged(ConnectionStatus::Connected); // Adapt this part to your desired callback method
}

static void disconnectedHandler(natsConnection* /*nc*/, void *closure) {
    // Clientstream* c = reinterpret_cast<Clientstream*>(closure);
    // emit c->statusChanged(ConnectionStatus::Disconnected); // Adapt this part to your desired callback method
}


Clientstream::Clientstream() : Clientstream("mystream", "myconsumer")
{

}

Clientstream::Clientstream(const char* consumername) : Clientstream("mystream", consumername) {

}

Clientstream::Clientstream(const char* streamname, const char* consumername) : m_streamname(streamname), m_consumername(consumername) {
    //initialize
    int cpuCoresCount = std::thread::hardware_concurrency();
    if (cpuCoresCount >= 2) {
        nats_SetMessageDeliveryPoolSize(cpuCoresCount);
    }
}

Clientstream::~Clientstream()
{
    close();
}

void Clientstream::connectToServer(const Options& opts)
{
    natsStatus          s;
    jsErrCode           jerr;

    natsOptions* nats_opts = buildNatsOptions(opts);
    std::unique_ptr<natsOptions, decltype(&natsOptions_Destroy)> optsPtr(nats_opts, &natsOptions_Destroy);

    natsOptions_UseGlobalMessageDelivery(nats_opts, true);
    natsOptions_SetErrorHandler(nats_opts, &errorHandler, this);
    natsOptions_SetClosedCB(nats_opts, &closedConnectionHandler, this);
    natsOptions_SetDisconnectedCB(nats_opts, &disconnectedHandler, this);
    natsOptions_SetReconnectedCB(nats_opts, &reconnectedHandler, this);

    // emit statusChanged(ConnectionStatus::Connecting); // Adapt this part to your desired callback method
    s = natsConnection_Connect(&m_conn, nats_opts);
    checkError(s);

    jsOptions_Init(&m_jsopts);
    s = natsConnection_JetStream(&m_jsCtx, m_conn, &m_jsopts);
    checkError(s);

    // emit statusChanged(ConnectionStatus::Connected); // Adapt this part to your desired callback method

    jsStreamConfig      jscfg;
    jsStreamConfig_Init(&jscfg);
    jscfg.Name = m_streamname;
    jscfg.Retention = js_LimitsPolicy;
    jscfg.Replicas = 2;
    jscfg.MaxMsgs = 1000;
    jscfg.MaxBytes = 1.5 * 1024 * 1024 * 1024; // 1.5 GB
    jscfg.MaxMsgSize = 200*1024*1204;
    jscfg.MaxAge = 3600 * 24 * 7 * (int64_t)1e9; // 1 week in nanoseconds

    // Check if the stream already exists
    s = js_GetStreamInfo(&m_jsinfo, m_jsCtx, jscfg.Name, NULL, &jerr);
    if (s == NATS_NOT_FOUND) {
        // Stream does not exist, create it
        s = js_AddStream(&m_jsinfo, m_jsCtx, &jscfg, NULL, &jerr);
        checkError(s,jerr);
    } else {
        checkError(s,jerr);  // Check for other errors
    }

    jsConsumerConfig    consumercfg;
    jsConsumerConfig_Init(&consumercfg);
    consumercfg.Durable = m_consumername;
    consumercfg.AckWait = (int64_t)10 * 1000 * 1000 * 1000; // 10秒
    consumercfg.DeliverPolicy = js_DeliverAll;
    consumercfg.AckPolicy = js_AckExplicit;

    // Check if the consumer already exists
    s = js_GetConsumerInfo(&m_consumerinfo, m_jsCtx, jscfg.Name, consumercfg.Durable, NULL, &jerr);
    if (s == NATS_NOT_FOUND) {
        // Consumer does not exist, create it
        s = js_AddConsumer(&m_consumerinfo, m_jsCtx, jscfg.Name, &consumercfg, NULL, &jerr);
        checkError(s,jerr);
    } else {
        checkError(s,jerr);  // Check for other errors
    }
}

void Clientstream::connectToServer(const std::string& address)
{
    Options connOpts;
    connOpts.servers.push_back(address);
    connectToServer(connOpts);
}

void Clientstream::connectToServer()
{
    Options connOpts;
    connOpts.servers.push_back(NATS_DEFAULT_URL);
    connectToServer(connOpts);
}

void Clientstream::close() noexcept
{
    if (m_jsinfo != NULL) {
        jsStreamInfo_Destroy(m_jsinfo);
        m_jsinfo = NULL;
    }

    if (m_consumerinfo != NULL) {
        jsConsumerInfo_Destroy(m_consumerinfo);
        m_consumerinfo = NULL;
    }
    if (m_pubAck != NULL) {
        jsPubAck_Destroy(m_pubAck);
        m_pubAck = NULL;
    }
    if (m_sub != NULL) {
        natsSubscription_Destroy(m_sub);
        m_sub = NULL;
    }
    if (m_jsCtx != NULL) {
        jsCtx_Destroy(m_jsCtx);
        m_jsCtx = NULL;
    }

    if (m_conn != NULL) {
        natsConnection_Close(m_conn);
        natsConnection_Destroy(m_conn);
        m_conn = nullptr;
    }

}

//todo:判断subject是否和stream中的subject重叠，通配符判断还没添加
bool Clientstream::isSubjectOverlap(const char* existingSubject, const char* newSubject) {
    std::string existing(existingSubject);
    std::string newS(newSubject);

    // 检查是否相等
    if (existing == newS) {
        return true;
    }

    // 如果其中一个是通配符，认为存在重叠
/*    if (existing.find('*') != std::string::npos || existing.find('>') != std::string::npos ||
        newS.find('*') != std::string::npos || newS.find('>') != std::string::npos) {
        return true;
        }
*/
    // 其他自定义重叠检查逻辑
    return false;
}

void Clientstream::addSubjectIfNotExists(const char* subject) {
    jsStreamConfig *streamConfig = m_jsinfo->Config;
     for (int i = 0; i < streamConfig->SubjectsLen; ++i) {
        if (strcmp(streamConfig->Subjects[i], subject) == 0) {
           return; // Subject 已经存在，不需要更新
        }
    }

    char **newSubjects = (char **) realloc(streamConfig->Subjects, sizeof(char *) * (streamConfig->SubjectsLen + 1));
    if (newSubjects == nullptr) {
        exit(1);
    }
    newSubjects[streamConfig->SubjectsLen] = strdup(subject);
    if (newSubjects[streamConfig->SubjectsLen] == nullptr) {
        exit(1);
    }
    streamConfig->Subjects = (const char **) newSubjects;
    streamConfig->SubjectsLen++;

    jsStreamInfo *updatedStreamInfo = nullptr;
    natsStatus          s;
    jsErrCode           jerr;
    s = js_UpdateStream(&updatedStreamInfo, m_jsCtx, streamConfig, nullptr, &jerr);
    checkError(s,jerr);

    jsStreamInfo_Destroy(m_jsinfo);
    m_jsinfo = updatedStreamInfo;
}

void Clientstream::addSubjectToConsumerIfNotExists(const char* newSubject, const int mode = 0) {
    natsStatus          s;
    jsErrCode           jerr;
    // 获取当前的 consumer 配置
    jsConsumerConfig* consumerConfig = m_consumerinfo->Config;

    // mode = 0 pull 模式，mode = 1 push 模式, 若模式发生改变，需要更新 consumer
    int modeChanged = (mode != 0 && consumerConfig->DeliverSubject == nullptr) ||
                      (mode == 0 && consumerConfig->DeliverSubject != nullptr);

    // 如果 FilterSubject 有值，转换它到 FilterSubjects 数组
    if (consumerConfig->FilterSubject != nullptr) {
        consumerConfig->FilterSubjects = (const char **) malloc(sizeof(char *));
        if (consumerConfig->FilterSubjects == nullptr) {
            exit(1);
        }
        consumerConfig->FilterSubjects[0] = strdup(consumerConfig->FilterSubject);
        if (consumerConfig->FilterSubjects[0] == nullptr) {
           free(consumerConfig->FilterSubjects); // 假设需要释放 FilterSubjects
           exit(1);
        }
        consumerConfig->FilterSubjectsLen = 1;
        free((void*)consumerConfig->FilterSubject);
        consumerConfig->FilterSubject = nullptr; // 清除单一过滤主题
    }

    // 检查新主题是否与现有主题重叠
    bool overlap = false;
    for (int i = 0; i < consumerConfig->FilterSubjectsLen; ++i) {
        if (isSubjectOverlap(consumerConfig->FilterSubjects[i], newSubject)) {
            // 新主题与现有主题重叠，不更新
            overlap = true;
            break;
        }
    }

    // 检查 newSubject 是否已经在 consumerConfig.FilterSubjects 中
    bool exists = false;
    for (int i = 0; i < consumerConfig->FilterSubjectsLen; ++i) {
        if (strcmp(consumerConfig->FilterSubjects[i], newSubject) == 0) {
            // Subject 已经存在，不需要更新
            exists = true;
            break;
        }
    }

    // 情况无变化，不更新
    if (!modeChanged && overlap && exists) {
        // printf("Subject %s already exists in the consumer\n", newSubject);
        return;
    }

    if (!overlap && !exists) {
        // 重新分配内存以容纳新的 subject
        char **newFilterSubjects = (char **)realloc(consumerConfig->FilterSubjects, sizeof(char *) * (consumerConfig->FilterSubjectsLen + 1));
        if (newFilterSubjects == nullptr){
            exit(1);
        }

        // 复制新的 subject 字符串并将其添加到 newFilterSubjects 数组中
        newFilterSubjects[consumerConfig->FilterSubjectsLen] = strdup(newSubject);
        if (newFilterSubjects[consumerConfig->FilterSubjectsLen] == nullptr){
            exit(1);
        }

        // 更新 consumerConfig
        consumerConfig->FilterSubjects = (const char **)newFilterSubjects;
        consumerConfig->FilterSubjectsLen += 1;
    }

    // 模式不变，更新consumer
    if (!modeChanged) {
        jsConsumerInfo *consumerinfo;
        s = js_UpdateConsumer(&consumerinfo, m_jsCtx, m_streamname, consumerConfig, NULL, &jerr);
        checkError(s,jerr);

        jsConsumerInfo_Destroy(m_consumerinfo);
        m_consumerinfo = consumerinfo;

        return;
    }

    // 模式改变，创建新的consumer
    if (modeChanged) {
        // printf("Creating new consumer for subject %s\n", newSubject);
        s = js_DeleteConsumer(m_jsCtx, m_streamname, consumerConfig->Durable, NULL, &jerr);

        checkError(s,jerr);
        // 检查是否删除了现有的消费者
        s = js_GetConsumerInfo(&m_consumerinfo, m_jsCtx, m_streamname, consumerConfig->Durable, NULL, &jerr);
        if (s != NATS_NOT_FOUND) {
            checkError(s,jerr);
        }

        // 确保资源完全释放
        // if (m_consumerinfo != nullptr) {
        //     jsConsumerInfo_Destroy(m_consumerinfo);
        //     m_consumerinfo = nullptr;
        // }
        // push 模式
        if (mode == 1) {
            consumerConfig->DeliverSubject = strdup(m_consumername);
            consumerConfig->MaxWaiting = 0;
        } else {
            if (consumerConfig->DeliverSubject)
                delete consumerConfig->DeliverSubject;
            consumerConfig->DeliverSubject = NULL;
        }

        jsConsumerInfo *consumerinfo;
        s = js_AddConsumer(&consumerinfo, m_jsCtx, m_streamname, consumerConfig, NULL, &jerr);

        checkError(s,jerr);

        jsConsumerInfo_Destroy(m_consumerinfo);
        m_consumerinfo = consumerinfo;
        m_consumername = consumerConfig->Durable;
    }
}
void Clientstream::publish(Message *msg){
    addSubjectIfNotExists(msg->getSubject().c_str());//检测主题在stream是否存在，不存在添加到stream上

    natsStatus          s;
    jsErrCode           jerr;
    natsMsg             *nmsg;
    s = msg->toNatsMsg(&nmsg);
    s = js_PublishMsg(&m_pubAck, m_jsCtx, nmsg, NULL, &jerr);
    checkError(s,jerr);
    natsMsg_Destroy(nmsg);
    };

void Clientstream::publish(const char* subject, const void* data, int len) {
    addSubjectIfNotExists(subject);//检测主题在stream是否存在，不存在添加到stream上

    natsStatus          s;
    jsErrCode           jerr;

    s = js_Publish(&m_pubAck, m_jsCtx, subject, (const void *)data, len, NULL, &jerr);
    checkError(s,jerr);
}

void Clientstream::PullSubscribe(const char* subject) {
    addSubjectToConsumerIfNotExists(subject);

    natsStatus          s;
    jsErrCode           jerr;

    jsSubOptions_Init(&m_subOpts);
    m_subOpts.Stream = m_streamname;
    m_subOpts.Consumer = m_consumername;

    s = js_PullSubscribe(&m_sub, m_jsCtx, subject, m_consumername, NULL, &m_subOpts, &jerr);
    checkError(s,jerr);
}

int Clientstream::receiveMessages(void** data,int* len) {
    natsMsgList list;
    natsStatus s;
    jsErrCode jerr;
    *data = NULL;

    // Fetch one message
    s = natsSubscription_Fetch(&list, m_sub, 1, 1000, &jerr);
    if (s == NATS_OK && list.Count > 0) {
        *len = natsMsg_GetDataLength(list.Msgs[0]);
        *data = (void *)malloc(*len + 1);
        if (*data != NULL) {
            memcpy(*data, natsMsg_GetData(list.Msgs[0]), *len);
            ((char *)(*data))[*len] = '\0'; // Null-terminate the data
         }
        s = natsMsg_Ack(list.Msgs[0], NULL);
        natsMsgList_Destroy(&list);
        return 1; // Message received
    } else if (s == NATS_TIMEOUT && list.Count == 0) {
        natsMsgList_Destroy(&list);
        return 0; // No more messages
    } else {
        checkError(s,jerr);
 //       printf("Error receiving message: %s\n", natsStatus_GetText(s));
        return -1; // Error
    }
}

int Clientstream::receiveMessages(Message **msg,bool ack)
{
    natsMsgList list;
    natsStatus s;
    jsErrCode jerr;

    // Fetch one message
    s = natsSubscription_Fetch(&list, m_sub, 1, 1000, &jerr);
    if (s == NATS_OK && list.Count > 0) {
        *msg = new Message(list.Msgs[0]);
        if(ack)
        {
            s = natsMsg_Ack(list.Msgs[0], NULL);
            checkError(s);
        }

        natsMsgList_Destroy(&list);
        return 1; // Message received

    } else if (s == NATS_TIMEOUT && list.Count == 0) {
        natsMsgList_Destroy(&list);
        return 0; // No more messages
    } else {
        checkError(s,jerr);
        //       printf("Error receiving message: %s\n", natsStatus_GetText(s));
        return -1; // Error
    }
}

/*************************Push 发布订阅模式************************/

void Clientstream::setMsgHandler(MessageHandler handler) {
    m_msgHandler = handler;
}

void Clientstream::internalMsgHandler(natsConnection* nc, natsSubscription* sub, natsMsg* msg, void* closure) {
    natsStatus s;
    Clientstream* c = reinterpret_cast<Clientstream*>(closure);
    Message clientmsg(msg);
    if (c->m_msgHandler) {
        c->m_msgHandler(clientmsg);
    }
    s = natsMsg_Ack(msg, NULL);
    checkError(s);
    natsMsg_Destroy(msg);
}

void Clientstream::PushSubscribe(const char* subject, MessageHandler handler) {
    addSubjectToConsumerIfNotExists(subject, 1);

    natsStatus          s;
    jsErrCode           jerr;

    jsSubOptions_Init(&m_subOpts);
    m_subOpts.Stream = m_streamname;
    m_subOpts.Consumer = m_consumername;

    setMsgHandler(handler);
    
    // 订阅主题，指定内部回调函数
    s = js_Subscribe(&m_sub, m_jsCtx, subject, internalMsgHandler, (void*)this, &m_jsopts, &m_subOpts, &jerr);
    if (s != NATS_OK) {
        const char *lastErr = nats_GetLastError(NULL);
        printf("Error subscribing to JetStream: %s\n", natsStatus_GetText(s));
        printf("Detailed error: %s\n", lastErr ? lastErr : "No detailed error information");
        if (lastErr)
            delete lastErr;
        checkError(s, jerr);
        return;
    }
    checkError(s, jerr);
}

/**************************End*****************************/



std::string Clientstream::currentServer() const
{
    char buffer[500];
    natsStatus s = natsConnection_GetConnectedUrl(m_conn, buffer, sizeof(buffer));
    if (s != NATS_OK) {
        return std::string();
    }
    return std::string(buffer);
}



static const char* jsErrCodeToString(jsErrCode code) {
    switch (code) {
        case JSAccountResourcesExceededErr:
            return "Resource limits exceeded for account";
        case JSBadRequestErr:
            return "Bad request";
        case JSClusterIncompleteErr:
            return "Incomplete results";
        case JSClusterNoPeersErr:
            return "No suitable peers for placement";
        case JSClusterNotActiveErr:
            return "JetStream not in clustered mode";
        case JSClusterNotAssignedErr:
            return "JetStream cluster not assigned to this server";
        case JSClusterNotAvailErr:
            return "JetStream system temporarily unavailable";
        case JSClusterNotLeaderErr:
            return "JetStream cluster can not handle request";
        case JSClusterRequiredErr:
            return "JetStream clustering support required";
        case JSClusterTagsErr:
            return "Tags placement not supported for operation";
        case JSConsumerCreateErr:
            return "General consumer creation failure string";
        case JSConsumerNameExistErr:
            return "Consumer name already in use";
        case JSConsumerNotFoundErr:
            return "Consumer not found";
        case JSSnapshotDeliverSubjectInvalidErr:
            return "Deliver subject not valid";
        case JSConsumerDurableNameNotInSubjectErr:
            return "Consumer expected to be durable but no durable name set in subject";
        case JSConsumerDurableNameNotMatchSubjectErr:
            return "Consumer name in subject does not match durable name in request";
        case JSConsumerDurableNameNotSetErr:
            return "Consumer expected to be durable but a durable name was not set";
        case JSConsumerEphemeralWithDurableInSubjectErr:
            return "Consumer expected to be ephemeral but detected a durable name set in subject";
        case JSConsumerEphemeralWithDurableNameErr:
            return "Consumer expected to be ephemeral but a durable name was set in request";
        case JSStreamExternalApiOverlapErr:
            return "Stream external api prefix must not overlap";
        case JSStreamExternalDelPrefixOverlapsErr:
            return "Stream external delivery prefix overlaps with stream subject";
        case JSInsufficientResourcesErr:
            return "Insufficient resources";
        case JSStreamInvalidExternalDeliverySubjErr:
            return "Stream external delivery prefix must not contain wildcards";
        case JSInvalidJSONErr:
            return "Invalid JSON";
        case JSMaximumConsumersLimitErr:
            return "Maximum consumers exceeds account limit";
        case JSMaximumStreamsLimitErr:
            return "Maximum number of streams reached";
        case JSMemoryResourcesExceededErr:
            return "Insufficient memory resources available";
        case JSMirrorConsumerSetupFailedErr:
            return "Generic mirror consumer setup failure";
        case JSMirrorMaxMessageSizeTooBigErr:
            return "Stream mirror must have max message size >= source";
        case JSMirrorWithSourcesErr:
            return "Stream mirrors can not also contain other sources";
        case JSMirrorWithStartSeqAndTimeErr:
            return "Stream mirrors can not have both start seq and start time configured";
        case JSMirrorWithSubjectFiltersErr:
            return "Stream mirrors can not contain filtered subjects";
        case JSMirrorWithSubjectsErr:
            return "Stream mirrors can not also contain subjects";
        case JSNoAccountErr:
            return "Account not found";
        case JSClusterUnSupportFeatureErr:
            return "Not currently supported in clustered mode";
        case JSNoMessageFoundErr:
            return "No message found";
        case JSNotEmptyRequestErr:
            return "Expected an empty request payload";
        case JSNotEnabledForAccountErr:
            return "JetStream not enabled for account";
        case JSClusterPeerNotMemberErr:
            return "Peer not a member";
        case JSRaftGeneralErr:
            return "General RAFT error";
        case JSRestoreSubscribeFailedErr:
            return "JetStream unable to subscribe to restore snapshot";
        case JSSequenceNotFoundErr:
            return "Sequence not found";
        case JSClusterServerNotMemberErr:
            return "Server is not a member of the cluster";
        case JSSourceConsumerSetupFailedErr:
            return "General source consumer setup failure";
        case JSSourceMaxMessageSizeTooBigErr:
            return "Stream source must have max message size >= target";
        case JSStorageResourcesExceededErr:
            return "Insufficient storage resources available";
        case JSStreamAssignmentErr:
            return "Generic stream assignment error";
        case JSStreamCreateErr:
            return "Generic stream creation error";
        case JSStreamDeleteErr:
            return "General stream deletion error";
        case JSStreamGeneralError:
            return "General stream failure";
        case JSStreamInvalidConfig:
            return "Stream configuration validation error";
        case JSStreamLimitsErr:
            return "General stream limits exceeded error";
        case JSStreamMessageExceedsMaximumErr:
            return "Message size exceeds maximum allowed";
        case JSStreamMirrorNotUpdatableErr:
            return "Mirror configuration can not be updated";
        case JSStreamMismatchErr:
            return "Stream name in subject does not match request";
        case JSStreamMsgDeleteFailed:
            return "Generic message deletion failure error";
        case JSStreamNameExistErr:
            return "Stream name already in use";
        case JSStreamNotFoundErr:
            return "Stream not found";
        case JSStreamNotMatchErr:
            return "Expected stream does not match";
        case JSStreamReplicasNotUpdatableErr:
            return "Replicas configuration can not be updated";
        case JSStreamRestoreErr:
            return "Restore failed";
        case JSStreamSequenceNotMatchErr:
            return "Expected stream sequence does not match";
        case JSStreamSnapshotErr:
            return "Snapshot failed";
        case JSStreamSubjectOverlapErr:
            return "Subjects overlap with an existing stream";
        case JSStreamTemplateCreateErr:
            return "Generic template creation failed";
        case JSStreamTemplateDeleteErr:
            return "Generic stream template deletion failed error";
        case JSStreamTemplateNotFoundErr:
            return "Template not found";
        case JSStreamUpdateErr:
            return "Generic stream update error";
        case JSStreamWrongLastMsgIDErr:
            return "Wrong last msg ID";
        case JSStreamWrongLastSequenceErr:
            return "Wrong last sequence";
        case JSTempStorageFailedErr:
            return "JetStream unable to open temp storage for restore";
        case JSTemplateNameNotMatchSubjectErr:
            return "Template name in subject does not match request";
        case JSStreamReplicasNotSupportedErr:
            return "Replicas > 1 not supported in non-clustered mode";
        case JSPeerRemapErr:
            return "Peer remap failed";
        case JSNotEnabledErr:
            return "JetStream not enabled";
        case JSStreamStoreFailedErr:
            return "Generic error when storing a message failed";
        case JSConsumerConfigRequiredErr:
            return "Consumer config required";
        case JSConsumerDeliverToWildcardsErr:
            return "Consumer deliver subject has wildcards";
        case JSConsumerPushMaxWaitingErr:
            return "Consumer in push mode can not set max waiting";
        case JSConsumerDeliverCycleErr:
            return "Consumer deliver subject forms a cycle";
        case JSConsumerMaxPendingAckPolicyRequiredErr:
            return "Consumer requires ack policy for max ack pending";
        case JSConsumerSmallHeartbeatErr:
            return "Consumer idle heartbeat needs to be >= 100ms";
        case JSConsumerPullRequiresAckErr:
            return "Consumer in pull mode requires explicit ack policy";
        case JSConsumerPullNotDurableErr:
            return "Consumer in pull mode requires a durable name";
        case JSConsumerPullWithRateLimitErr:
            return "Consumer in pull mode can not have rate limit set";
        case JSConsumerMaxWaitingNegativeErr:
            return "Consumer max waiting needs to be positive";
        case JSConsumerHBRequiresPushErr:
            return "Consumer idle heartbeat requires a push based consumer";
        case JSConsumerFCRequiresPushErr:
            return "Consumer flow control requires a push based consumer";
        case JSConsumerDirectRequiresPushErr:
            return "Consumer direct requires a push based consumer";
        case JSConsumerDirectRequiresEphemeralErr:
            return "Consumer direct requires an ephemeral consumer";
        case JSConsumerOnMappedErr:
            return "Consumer direct on a mapped consumer";
        case JSConsumerFilterNotSubsetErr:
            return "Consumer filter subject is not a valid subset of the interest subjects";
        case JSConsumerInvalidPolicyErr:
            return "Generic delivery policy error";
        case JSConsumerInvalidSamplingErr:
            return "Failed to parse consumer sampling configuration";
        case JSStreamInvalidErr:
            return "Stream not valid";
        case JSConsumerWQRequiresExplicitAckErr:
            return "Workqueue stream requires explicit ack";
        case JSConsumerWQMultipleUnfilteredErr:
            return "Multiple non-filtered consumers not allowed on workqueue stream";
        case JSConsumerWQConsumerNotUniqueErr:
            return "Filtered consumer not unique on workqueue stream";
        case JSConsumerWQConsumerNotDeliverAllErr:
            return "Consumer must be deliver all on workqueue stream";
        case JSConsumerNameTooLongErr:
            return "Consumer name is too long";
        case JSConsumerBadDurableNameErr:
            return "Durable name can not contain '.', '*', '>'";
        case JSConsumerStoreFailedErr:
            return "Error creating store for consumer";
        case JSConsumerExistingActiveErr:
            return "Consumer already exists and is still active";
        case JSConsumerReplacementWithDifferentNameErr:
            return "Consumer replacement durable config not the same";
        case JSConsumerDescriptionTooLongErr:
            return "Consumer description is too long";
        case JSConsumerWithFlowControlNeedsHeartbeatsErr:
            return "Consumer with flow control also needs heartbeats";
        case JSStreamSealedErr:
            return "Invalid operation on sealed stream";
        case JSStreamPurgeFailedErr:
            return "Generic stream purge failure";
        case JSStreamRollupFailedErr:
            return "Generic stream rollup failure";
        case JSConsumerInvalidDeliverSubjectErr:
            return "Invalid push consumer deliver subject";
        case JSStreamMaxBytesRequiredErr:
            return "Account requires a stream config to have max bytes set";
        case JSConsumerMaxRequestBatchNegativeErr:
            return "Consumer max request batch needs to be > 0";
        case JSConsumerMaxRequestExpiresToSmallErr:
            return "Consumer max request expires needs to be > 1ms";
        case JSConsumerMaxDeliverBackoffErr:
            return "Max deliver is required to be > length of backoff values";
        case JSStreamInfoMaxSubjectsErr:
            return "Subject details would exceed maximum allowed";
        case JSStreamOfflineErr:
            return "Stream is offline";
        case JSConsumerOfflineErr:
            return "Consumer is offline";
        case JSNoLimitsErr:
            return "No JetStream default or applicable tiered limit present";
        case JSConsumerMaxPendingAckExcessErr:
            return "Consumer max ack pending exceeds system limit";
        case JSStreamMaxStreamBytesExceededErr:
            return "Stream max bytes exceeds account limit max stream bytes";
        case JSStreamMoveAndScaleErr:
            return "Cannot move and scale a stream in a single update";
        case JSStreamMoveInProgressErr:
            return "Stream move already in progress";
        case JSConsumerMaxRequestBatchExceededErr:
            return "Consumer max request batch exceeds server limit";
        case JSConsumerReplicasExceedsStreamErr:
            return "Consumer config replica count exceeds parent stream";
        case JSConsumerNameContainsPathSeparatorsErr:
            return "Consumer name can not contain path separators";
        case JSStreamNameContainsPathSeparatorsErr:
            return "Stream name can not contain path separators";
        case JSStreamMoveNotInProgressErr:
            return "Stream move not in progress";
        case JSStreamNameExistRestoreFailedErr:
            return "Stream name already in use, cannot restore";
        case JSConsumerCreateFilterSubjectMismatchErr:
            return "Consumer create request did not match filtered subject from create subject";
        case JSConsumerCreateDurableAndNameMismatchErr:
            return "Consumer Durable and Name have to be equal if both are provided";
        case JSReplicasCountCannotBeNegativeErr:
            return "Replicas count cannot be negative";
        default:
            return "Unknown error code";
    }
}

void ARipple::checkError(natsStatus s,jsErrCode jerr)
{
    if (s == NATS_OK) return;
    std::string error;
    if(jerr == 0) {
        error = natsStatus_GetText(s);
    }
    else {
        error = jsErrCodeToString(jerr);
    }

    throw Exception(error);
}