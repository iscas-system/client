/**
 * Copyright (2022, ) Institute of Software, Chinese Academy of Sciences
 */
package com.gitee.fleet.client.yamls;

import com.gitee.fleet.kube.writers.DeploymentWriter;
import com.gitee.fleet.kube.writers.PVCWriter;
import com.gitee.fleet.kube.writers.PVWriter;
import com.gitee.fleet.kube.writers.SecretWriter;
import com.gitee.fleet.kube.writers.ServiceWriter;
import com.gitee.fleet.kube.writers.WorkloadWriter.Container;
import com.gitee.fleet.kube.writers.WorkloadWriter.Env;
import com.gitee.fleet.kube.writers.WorkloadWriter.Port;
import com.gitee.fleet.kube.writers.WorkloadWriter.VolumeMount;
import com.gitee.fleet.kube.writers.WorkloadWriter.Env.ValueFrom;
import com.gitee.fleet.kube.writers.WorkloadWriter.Env.ValueFrom.SecretKeyRef;

/**
 * @author wuheng@iscas.ac.cn
 * @since  2023/08/02
 * @version 1.0.3
 *
 * get real Url from <code>KubernetesRuleBase</code>
 * 
 */
public class KubeDatabaseTest {
	
	static final String NAME = "kube-database";
	
	static final String POSTGRES = "postgres";
	
	static final String POSTGRES_IMAGE = "postgres:15.3-alpine";
	
	static final String ADMINER = "adminer";
	
	static final String ADMINER_IMAGE = "adminer:4.8.1-standalone";
	
	public static void main(String[] args) throws Exception {
		SecretWriter secret = new SecretWriter(NAME, StackCommon.NAMESPACE);
		secret.withData(StackCommon.CONFIG_PASSWORD, StackCommon.base64("onceas")).stream(System.out);
		
		PVWriter pv = new PVWriter(NAME);
		
		pv.withCapacity("20").withPath(StackCommon.PATH + POSTGRES).withPVC(NAME, StackCommon.NAMESPACE).stream(System.out);
		
		PVCWriter pvc = new PVCWriter(NAME, StackCommon.NAMESPACE);
		pvc.withCapacity("20").stream(System.out);
		
		DeploymentWriter deploy = new DeploymentWriter(NAME, StackCommon.NAMESPACE);
		
		deploy.withMasterEnbale()
				.withContainer(new Container(POSTGRES, POSTGRES_IMAGE, 
						//POSTGRES_USER
								new Env[] {
										new Env("POSTGRES_PASSWORD", new ValueFrom(
												new SecretKeyRef(NAME, StackCommon.CONFIG_PASSWORD)))}, 
								new Port[] {
										new Port(5432)
								}, 
								new VolumeMount[] {
										new VolumeMount(StackCommon.VOLUME_DATA, "/var/lib/postgresql")
								}))
				.withContainer(new Container(ADMINER, ADMINER_IMAGE, 
								null, 
								new Port[] {
										new Port(8080)
								}, 
								new VolumeMount[] {
										new VolumeMount(StackCommon.VOLUME_DATA, "/var/lib/postgresql")
								}))
				.withPVCVolume(StackCommon.VOLUME_DATA, NAME)
		.stream(System.out);
		
		ServiceWriter service = new ServiceWriter(NAME, StackCommon.NAMESPACE);
		service.withType("NodePort").withSelector(NAME).withPort(5432, 30306, POSTGRES)
				.withPort(8080, 30307, ADMINER).stream(System.out);
	}

}
