/**
 * Copyright (2022, ) Institute of Software, Chinese Academy of Sciences
 */
package com.gitee.fleet.client.yamls;


import com.gitee.fleet.kube.writers.DeploymentWriter;
import com.gitee.fleet.kube.writers.ServiceWriter;
import com.gitee.fleet.kube.writers.WorkloadWriter.Container;
import com.gitee.fleet.kube.writers.WorkloadWriter.Port;

/**
 * @author wuheng@iscas.ac.cn
 * @since  2023/08/02
 * @version 1.0.3
 *
 * get real Url from <code>KubernetesRuleBase</code>
 * 
 */
public class KubeBackendTest {
	
	static final String NAME = "kube-backend";
	
	static final String BACKEND = "backend";
	
	static final String BACKEND_IMAGE = "registry.cn-beijing.aliyuncs.com/dosproj/backend:v1.2.3";
	
	public static void main(String[] args) throws Exception {
		
		DeploymentWriter deploy = new DeploymentWriter(NAME, StackCommon.NAMESPACE);
		
		deploy.withMasterEnbale()
				.withContainer(new Container(BACKEND, BACKEND_IMAGE, 
								null, 
								new Port[] {
										new Port(30308)
								}, 
								null))
		.stream(System.out);
		
		ServiceWriter service = new ServiceWriter(NAME, StackCommon.NAMESPACE);
		service.withType("NodePort").withSelector(NAME)
				.withPort(30308, 30308, "backend")
				.stream(System.out);
	}
}
