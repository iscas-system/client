/**
 * Copyright (2020, ) Institute of Software, Chinese Academy of Sciences
 */
package com.gitee.fleet.client.unit;

import java.io.File;

import com.gitee.fleet.kube.KubernetesAnalyzer;
import com.gitee.fleet.kube.KubernetesClient;

/**
 * @author wuheng09@gmail.com
 *
 */
public abstract class AbstractClient {

	public static KubernetesClient createClient1(KubernetesAnalyzer ana) throws Exception {
		return (ana == null) ? new KubernetesClient(System.getenv("url"), System.getenv("token")) : 
						new KubernetesClient(System.getenv("url"), System.getenv("token"), ana);
	}
	
	public static KubernetesClient createClient3(KubernetesAnalyzer ana) throws Exception {
		return (ana == null) ? new KubernetesClient(new File("admin.conf")) : new KubernetesClient(new File("admin.conf"), ana);
	}

}
