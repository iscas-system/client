/**
 * Copyright (2024, ) Institute of Software, Chinese Academy of Sciences
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitee.fleet.utils;

import org.apache.hc.client5.http.classic.methods.HttpDelete;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.classic.methods.HttpPut;
import org.apache.hc.client5.http.classic.methods.HttpUriRequestBase;

/**
 * 
 * It is used for creating various HttpRequest
 * 
 * @author wuheng@iscas.ac.cn
 * @since 1.0.0
 **/
public class HttpUtil {

	public static final String POST    = "POST";
	
	public static final String DELETE  = "DELETE";
	
	public static final String PUT     = "PUT";
	
	public static final String GET     = "GET";
	
	/**********************************************************
	 * 
	 * Commons
	 * 
	 **********************************************************/

	private HttpUtil() {
		super();
	}

	public static String getHttpType(HttpUriRequestBase request) {
		if (request instanceof HttpGet) {
            return GET;
        } else if (request instanceof HttpPost) {
            return POST;
        } else if (request instanceof HttpDelete) {
            return DELETE;
        } else if (request instanceof HttpPut) {
            return PUT;
        }
        
		throw new UnsupportedOperationException("only supports GET, POST, DELETE and PUT, but now is " + request);
	}

	/**
	 * @param uri
	 * @return
	 */
	public static HttpUriRequestBase from(String uri, String type) {
		if (POST.equals(type)) {
			return new HttpPost(uri);
		} else if (GET.equals(type)) {
			return new HttpGet(uri);
		} else if (PUT.equals(type)) {
			return new HttpPut(uri);
		} else if (DELETE.equals(type)) {
			return new HttpDelete(type);
		} else {
			throw new UnsupportedOperationException(
					"only support POST, GET, PUT and DELETE");
		}
	}
}
