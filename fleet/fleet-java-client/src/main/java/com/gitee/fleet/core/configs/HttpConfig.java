/**
 * Copyright (2024, ) Institute of Software, Chinese Academy of Sciences
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *limitations under the License.
 */
package com.gitee.fleet.core.configs;

import org.apache.hc.client5.http.classic.methods.HttpUriRequestBase;

import com.gitee.fleet.core.AbstractConfig;

/**
 * Http的配置文件，又可细分为多种
 * - username和password
 * - token
 * - cookie
 * - cert
 * 
 * @author wuheng@iscas.ac.cn
 * @since 1.0.0
 * 
 */
public abstract class HttpConfig implements AbstractConfig {

	/**
	 * request request
	 */
	protected final HttpUriRequestBase request;


	protected HttpConfig(HttpUriRequestBase request) {
		super();
		this.request = request;
	}


	public HttpUriRequestBase getRequest() {
		return request;
	}

}
