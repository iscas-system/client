/**
 * Copyright (2024, ) Institute of Software, Chinese Academy of Sciences
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *limitations under the License.
 */
package com.gitee.fleet.core.configs.http;

import org.apache.hc.client5.http.classic.methods.HttpUriRequestBase;

import com.gitee.fleet.core.configs.HttpConfig;

/**
 * token配置文件
 * 
 * @author wuheng@iscas.ac.cn
 * @since 1.0.0
 * 
 */
public class TokenConfig extends HttpConfig {

	protected final String token;
	

	public TokenConfig(HttpUriRequestBase request, String token) {
		super(request);
		this.token = token;
	}


	public String getToken() {
		return token;
	}

}
