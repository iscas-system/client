/**
 * Copyright (2024, ) Institute of Software, Chinese Academy of Sciences
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitee.fleet;

import java.io.IOException;

import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;

import com.gitee.fleet.core.configs.http.TokenConfig;
import com.gitee.fleet.protocols.HttpProtocol;
import com.gitee.fleet.utils.HttpUtil;

/**
 * 
 * It is used for creating various HttpRequest
 * 
 * @author wuheng@iscas.ac.cn
 * @since 1.0.0
 **/
public class Main {

	public static void main(String[] args) throws IOException, ParseException {
		TokenConfig cookie = new TokenConfig(
				HttpUtil.from("", HttpUtil.GET), 
				"f95e6e5f-c623-400e-b0b1-b266e0e88e56");
		
		HttpGet httpGet = HttpProtocol.get(cookie);
		try (CloseableHttpResponse response = HttpClients.createDefault().execute(httpGet)) {
            
            // 获取响应状态码
            int statusCode = response.getCode();
            System.out.println("Response Status Code: " + statusCode);
            
            // 获取响应实体
            HttpEntity entity = response.getEntity();
            
            if (entity != null) {
                // 将响应实体转换为字符串并打印
                String responseBody = EntityUtils.toString(entity);
                System.out.println("Response Body: " + responseBody);
            }
        }
	}
}
