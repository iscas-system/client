/**
 * Copyright (2023, ) Institute of Software, Chinese Academy of Sciences
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitee.fleet.protocols;

import java.net.MalformedURLException;
import java.util.Base64;

import org.apache.hc.client5.http.classic.methods.HttpDelete;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.classic.methods.HttpPut;
import org.apache.hc.client5.http.classic.methods.HttpUriRequestBase;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.io.entity.StringEntity;

import com.gitee.fleet.core.configs.HttpConfig;
import com.gitee.fleet.core.configs.http.CookieConfig;
import com.gitee.fleet.core.configs.http.TokenConfig;
import com.gitee.fleet.core.configs.http.UsernameConfig;

/**
 * 
 * It is used for creating various HttpRequest
 * 
 * @author wuheng@iscas.ac.cn
 * @since 1.0.5
 **/
public class HttpProtocol {

	/**********************************************************
	 * 
	 * Commons
	 * 
	 **********************************************************/

	private HttpProtocol() {
		super();
	}

	/**
	 * @param config config
	 * @param body  body
	 * @return request
	 */
	private static HttpUriRequestBase createRequest(HttpConfig config, String body) {
		HttpUriRequestBase request = config.getRequest();
		setBodyIfNeeds(body, request);
		setHeaders(config, request);
		
		return request;
	}

	public static void setHeaders(HttpConfig config, HttpUriRequestBase request) {
		if (config instanceof CookieConfig) {
			setCookie(request, ((CookieConfig) config).getCookie());
		} else if (config instanceof TokenConfig) {
			setToken(request, ((TokenConfig) config).getToken());
		} else if (config instanceof UsernameConfig) {
			setUser(request, ((UsernameConfig) config).getUsername(),
					((UsernameConfig) config).getPassword());
		} else {
			throw new UnsupportedOperationException("unsupport operations");
		}
	}

	/**
	 * @param body
	 * @param request
	 */
	public static void setBodyIfNeeds(String body, HttpUriRequestBase request) {
		try {
			request.setEntity(new StringEntity(
					body == null ? "" : body, ContentType.APPLICATION_JSON));
		} catch (Exception ex) {
			// ignore here
		}
	}
	
	/**
	 * @param request request
	 * @param token   token
	 */
	private static void setToken(HttpUriRequestBase request, String token) {
		request.addHeader("Authorization", "Bearer " + token);
		request.addHeader("Connection", "keep-alive");
	}
	
	
	/**
	 * @param request
	 * @param username
	 * @param password
	 */
	private static void setUser(HttpUriRequestBase request, String username, String password) {
		request.addHeader("Authorization", "Basic " 
							+ Base64.getEncoder().encodeToString(
								(username + ":" + password).getBytes()));
		request.addHeader("Connection", "keep-alive");
	}
	
	
	/**
	 * @param request
	 * @param cookie
	 */
	private static void setCookie(HttpUriRequestBase request, String cookie) {
		
		request.addHeader("Connection", "keep-alive");
		request.addHeader("Cookie", cookie);
	}
	

	/**********************************************************
	 * 
	 * Core
	 * 
	 **********************************************************/
	/**
	 * @param config config
	 * @param uri   uri
	 * @param body  body
	 * @return request or null
	 * @throws MalformedURLException MalformedURLException
	 */
	public static HttpPost post(HttpConfig config, String body) {
		return (HttpPost) createRequest(config, body);
	}

	/**
	 * @param config config
	 * @param uri   uri
	 * @param body  body
	 * @return request or null
	 * @throws MalformedURLException MalformedURLException
	 */
	public static HttpPut put(HttpConfig config, String body) {
		return (HttpPut) createRequest(config, body);
	}

	/**
	 * @param config config
	 * @param uri   uri
	 * @return request or null
	 * @throws MalformedURLException MalformedURLException
	 */
	public static HttpDelete delete(HttpConfig config) {
		return (HttpDelete) createRequest(config, null);
	}

	/**
	 * @param config config
	 * @param uri   uri
	 * @return request or null
	 * @throws MalformedURLException MalformedURLException
	 */
	public static HttpGet get(HttpConfig config)  {
		return (HttpGet) createRequest(config, null);
	}

}
