# libaripple
发布订阅基础服务（ARipple）的客户端，拟支持消息、流式数据、文件、对象存储、关系型数据库等多种类型数据的发布订阅。

目前基于nats二次开发。

## 依赖
- nats.c: nats
```shell
# For openEuler 22.03 LTS SP3 and openEuler 24.03 LTS (x86-64)
# Requires cmake, make, gcc, openssl-devel libprotobuf-c
$ git clone git@github.com:nats-io/nats.c.git
$ cd nats.c
$ mkdir build
$ cd build
$ cmake ..
$ make install
```
动态链接库的默认位置是 `/usr/local/lib64`，需要将路径加入LD的查找路径（如`/etc/ld.so.conf`）

- libprotobuf-c: 被nats.c依赖，是Google Protocol Buffer的C语言实现
```shell
# For openEuler 22.03 LTS SP3 and openEuler 24.03 LTS (x86-64)
# Requires protobuf-devel
$ wget https://github.com/protobuf-c/protobuf-c/releases/download/v1.5.0/protobuf-c-1.5.0.tar.gz
$ tar -zxvf protobuf-c-1.5.0.tar.gz
$ cd protobuf-c-1.5.0
$ ./configure
$ make
$ make install
```

- PostgreSQL: 关系型数据库引擎
```shell
# For openEuler 22.03 LTS SP3 and openEuler 24.03 LTS (x86-64)
$ dnf install postgresql
```

- sqlite: 轻量级嵌入式关系型数据库引擎
```shell
# For openEuler 22.03 LTS SP3 and openEuler 24.03 LTS (x86-64)
$ dnf install sqlite-devel
```

## 编译安装
- 执行`make`命令后将创建release目录并生成动态库
- 拥有root权限时可执行`make install`将动态库和头文件安装至系统目录
- 编译时添加参数`-laripple`即可链接动态库
- 不具有root权限时，可将动态库复制到适当的位置，并在编译时传入`-Wl,-rpath=`参数以指定动态库位置