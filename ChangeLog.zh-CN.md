# libaripple
发布订阅基础服务（ARipple）的客户端
- [v0.1](#v01)
## v0.1
- 提供C++客户端动态链接库
- 支持消息的发布-订阅模式
- 支持流式数据的生产者-消费者模式
- 支持通过消息引擎代理SQL语句完成SQLite数据库同步
- 支持通过流式引擎传输小尺寸文件