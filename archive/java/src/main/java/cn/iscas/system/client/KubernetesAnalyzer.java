/**
 * Copyright (2022, ) Institute of Software, Chinese Academy of Sciences
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.iscas.system.client;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import cn.iscas.system.client.cores.KubernetesConvertor;
import cn.iscas.system.client.cores.KubernetesRegistry;
import cn.iscas.system.client.cores.KubernetesRuleBase;
import cn.iscas.system.client.exceptions.KubernetesBadRequestException;
import cn.iscas.system.client.exceptions.KubernetesConflictResourceException;
import cn.iscas.system.client.exceptions.KubernetesForbiddenAccessException;
import cn.iscas.system.client.exceptions.KubernetesInternalServerErrorException;
import cn.iscas.system.client.exceptions.KubernetesResourceNotFoundException;
import cn.iscas.system.client.exceptions.KubernetesUnauthorizedTokenException;
import cn.iscas.system.client.exceptions.KubernetesUnknownException;
import cn.iscas.system.client.utils.KubeUtil;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.http.HttpClient;
import io.fabric8.kubernetes.client.http.HttpRequest;
import io.fabric8.kubernetes.client.http.HttpResponse;

/**
 * Kubernetes分析器，自动分析Kubernetes中所有kind资源及URL，规则参见
 * https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27 
 * <br>
 * <br>
 * <table border=0 cellspacing=3 cellpadding=0 ">
 *     <tr style="background-color: rgb(204, 204, 255);">
 *         <th align=left>方法名
 *         <th align=left>请求类型
 *         <th align=left>访问地址
 *     <tr>
 *         <td>create
 *         <td>POST
 *         <td>/api/v1/namespaces/{namespace}/pods
 *     <tr>
 *         <td>update
 *         <td>PUT
 *         <td>/api/v1/namespaces/{namespace}/pods/{name}
 *     <tr>
 *         <td>delete
 *         <td>DELETE
 *         <td>/api/v1/namespaces/{namespace}/pods
 *     <tr>
 *         <td>get
 *         <td>GET
 *         <td>/api/v1/namespaces/{namespace}/pods/{name}
 *     <tr>
 *         <td>list
 *         <td>GET
 *         <td>/api/v1/pods{name}
 *     <tr>
 *         <td>watch
 *         <td>GET
 *         <td>/api/v1/watch/pods
 * </table>
 * 
 * @author wuheng@iscas.ac.cn
 *
 */
public final class KubernetesAnalyzer {

	/**
	 * 单例模式
	 */
	private static KubernetesAnalyzer analyzer = null;
	
	/**
	 * 日志
	 */
	private static final Logger m_logger = Logger.getLogger(KubernetesAnalyzer.class.getName());

	
	/**
	 * Kubernetes的URL规则库
	 */
	protected static final KubernetesRuleBase ruleBase = new KubernetesRuleBase();

	/**
	 * Kubernetes客户端
	 */
	protected final KubernetesClient client;
	
	/**
	 * Kubernetes转化器
	 */
	protected final KubernetesConvertor convertor;

	/**
	 * Kubernetes注册器
	 */
	protected final KubernetesRegistry registry;
	
	/*******************************************
	 * 
	 * Init
	 * 
	 ********************************************/
	/**
	 * 创建Kubernetes分析器
	 */
	public KubernetesAnalyzer(KubernetesClient client) {
		this.client = client;
		this.registry = new KubernetesRegistry(client, ruleBase);
		this.convertor = new KubernetesConvertor(client, ruleBase);
	}

	/**
	 * 
	 * 
	 * @throws Exception
	 */
	public void start() throws Exception {
		
		HttpClient httpClient = client.getHttpClient();
		
		HttpRequest httpReq = httpClient.newHttpRequestBuilder()
							.url(client.getMasterUrl()).build();
		HttpResponse<InputStream> httpResp  = httpClient.sendAsync(
								httpReq, InputStream.class).get();
		
		Iterator<JsonNode> iterator = KubeUtil.streamToJson(httpResp).get(
						KubernetesConstants.HTTP_RESPONSE_PATHS).iterator();
		
		// traverse all paths in key 'paths' 
		while (iterator.hasNext()) {
			
			String path = iterator.next().asText();
			
			// we just find and register Kubernetes native kinds
			// which cannot be undeployed
			if (path.startsWith(KubernetesConstants.KUBEAPI_CORE_PREFIX_PATTERN) && 
					(path.split(KubernetesConstants.KUBEAPI_PATHSEPARTOR_PATTERN).length == 4 
						|| path.equals(KubernetesConstants.KUBEAPI_CORE_PATTERN))) {

				// register it
				try {
					registry.registerKinds(path);
				} catch (Exception ex) {
					// warning
					m_logger.warning(ex.toString());
				}
			}
		}
	}
	
	
	/**
	 * https://appwrite.io/docs/advanced/platform/response-codes
	 * 
	 * 200 OK: 请求成功，服务器成功处理了请求并返回所请求的数据。 <br>
	 * 201 Created: 请求成功，服务器创建了新资源。 <br>
	 * 204 No Content: 请求成功，服务器处理成功，但没有返回数据。<br> 
	 * 400 Bad Request: 请求无效，服务器无法理解请求。 <br>
	 * 401 Unauthorized: 未授权，需要进行身份验证或令牌无效。 <br> 
	 * 403 Forbidden: 请求被拒绝，客户端没有访问资源的权限。 <br>
	 * 404 Not Found: 请求的资源不存在。 <br>
	 * 409 Conflict: 请求冲突，通常用于表示资源的当前状态与请求的条件不匹配。 <br> 
	 * 500 Internal Server Error: 服务器内部错误，表示服务器在处理请求时遇到了问题。 <br>
	 * 
	 * @param response 文件流
	 * @return json    json文件
	 * @throws Exception 即200到500异常 
	 */
	synchronized JsonNode checkResponse(HttpResponse<InputStream> response) throws Exception {

		switch (response.code()) {
		case 200:
		case 201:
			try {
				ObjectMapper objectMapper = new ObjectMapper();
				objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
				return objectMapper.readTree(response.body());
			} catch (Exception e) {
				throw new KubernetesUnknownException(e.toString());
			}
		case 400:
			throw new KubernetesBadRequestException(response.bodyString());
		case 401:
			throw new KubernetesUnauthorizedTokenException(response.bodyString());
		case 403:
			throw new KubernetesForbiddenAccessException(response.bodyString());
		case 404:
			throw new KubernetesResourceNotFoundException(response.bodyString());
		case 409:
			throw new KubernetesConflictResourceException(response.bodyString());
		case 500:
			throw new KubernetesInternalServerErrorException(response.bodyString());
		default:
			throw new KubernetesUnknownException(response.bodyString());
		}

	}
	
	/**
	 * @param client client
	 * @return KubernetesAnalyzer
	 * @throws Exception Exception
	 */
	public static KubernetesAnalyzer createKubernetesAnalyzer(KubernetesClient client) {
		if (analyzer == null) {
			analyzer = new KubernetesAnalyzer(client);
		}
		return analyzer;
	}

	/*******************************************
	 * 
	 * Getter
	 * 
	 ********************************************/

	/**
	 * @return 转换器
	 */
	public KubernetesConvertor getConvertor() {
		return convertor;
	}

	/**
	 * @return 注册器
	 */
	public KubernetesRegistry getRegistry() {
		return registry;
	}

}
